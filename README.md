# README #
Antes de executar o projeto, verifique se a connection string localizada dentro o projeto "WcfService" está alinhada com a instancia local da máquina.

### Comportamentos do sistema ###
* Ao conectar, tentará localizar o banco de dados.
Caso não o encontre, o criará.
* Caso não tenha nenhum prêmio cadastrado no sistema, nenhum cupom gerado será sorteado.
* Caso tente cadastrar um cliente no sistema com um CPF já existente, receberá uma exception informando deste problema.

### Funcionalidades ###
* Cadastro de clientes
* Cadastro de prêmios
* Cadastro de cupons
* Sorteio de cupons
* Sorteio de prêmios
* Relatórios diversos

### Tecnologias / Arquiteturas utilizadas ###
* C#
* WCF
* MVC
* DDD
* Bootstrap
* Sql Server 2012
* .Net Framework 4.5.2
* Visual Studio 2015
* CSS
* HTML5