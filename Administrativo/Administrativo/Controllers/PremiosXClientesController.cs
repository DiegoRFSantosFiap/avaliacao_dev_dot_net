﻿using System.Linq;
using System.Web.Mvc;

namespace Administrativo.Controllers
{
    public class PremiosXClientesController : Controller
    {
        // GET: PremiosXClientes
        public ActionResult Index()
        {
            ViewBag.data = new CouponServiceReference.CouponServiceClient().GetAll().Where(_ => _.IsAwarded).ToList();
            return View();
        }
    }
}