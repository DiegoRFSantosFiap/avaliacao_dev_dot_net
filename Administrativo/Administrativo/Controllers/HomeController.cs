﻿using System.Linq;
using System.Web.Mvc;

namespace Administrativo.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Clients = new ServiceReference.ClientServiceClient().GetAll().OrderByDescending(_ => _.Id).Take(5);
            ViewBag.Prizes = new PrizeServiceReference.PrizeServiceClient().GetAll().OrderByDescending(_ => _.Id).Take(5);
            return View();
        }
    }
}