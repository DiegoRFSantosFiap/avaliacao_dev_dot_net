﻿using System.Linq;
using System.Web.Mvc;

namespace Administrativo.Controllers
{
    public class PremiosController : Controller
    {
        // GET: Premios
        public ActionResult Index()
        {
            ViewBag.Message = Request.QueryString["msg"]?.ToString();
            ViewBag.Prizes = new PrizeServiceReference.PrizeServiceClient().GetAll().OrderBy(_ => _.Description).ToList();
            return View();
        }

        public void Novo(string Description)
        {
            new PrizeServiceReference.PrizeServiceClient().Create(new Domain.Restaurant.Prize()
            {
                Description = Description
            });

            Response.Redirect("../Premios?msg=Success");
        }

        public void Remover(int PrizeId)
        {
            new PrizeServiceReference.PrizeServiceClient().Remove(PrizeId);

            Response.Redirect("../Premios?msg=PrizeRemoved");
        }
    }
}