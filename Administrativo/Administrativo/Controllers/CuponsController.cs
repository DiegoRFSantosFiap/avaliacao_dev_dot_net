﻿using System;
using System.Web.Mvc;

namespace Administrativo.Controllers
{
    public class CuponsController : Controller
    {
        // GET: Cupons
        public ActionResult Index(int Cliente)
        {
            ViewBag.Message = Request.QueryString["msg"]?.ToString();
            ViewBag.Coupons = new CouponServiceReference.CouponServiceClient().GetAllByclient(Cliente);
            ViewBag.ClientId = Cliente;
            return View();
        }

        public void Novo(int ClientId)
        {
            try
            {
                var client = new ServiceReference.ClientServiceClient().GetBy(ClientId);
                var coupon = new Domain.Client.Coupon()
                {
                    CreationDate = DateTime.Now,
                    Client = client
                };

                new CouponServiceReference.CouponServiceClient().Create(coupon);

                Response.Redirect("../Cupons?Cliente=" + ClientId + "&msg=Success");
            }
            catch (Exception)
            {
                Response.Redirect("../Cupons?Cliente=" + ClientId + "&msg=Error");
            }
        }
    }
}