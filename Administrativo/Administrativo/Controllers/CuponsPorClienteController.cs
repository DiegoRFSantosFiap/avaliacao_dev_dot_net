﻿using System.Linq;
using System.Web.Mvc;

namespace Administrativo.Controllers
{
    public class CuponsPorClienteController : Controller
    {
        // GET: CuponsPorCliente
        public ActionResult Index()
        {
            ViewBag.Coupons = new CouponServiceReference.CouponServiceClient().GetAll()?.OrderBy(_ => _.Client.Name).ToList();
            return View();
        }
    }
}