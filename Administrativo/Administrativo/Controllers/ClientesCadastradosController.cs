﻿using System.Linq;
using System.Web.Mvc;

namespace Administrativo.Controllers
{
    public class ClientesCadastradosController : Controller
    {
        // GET: ClientesCadastrados
        public ActionResult Index()
        {
            ViewBag.Clients = new ServiceReference.ClientServiceClient().GetAll().OrderBy(_ => _.Name).ToList();
            return View();
        }
    }
}