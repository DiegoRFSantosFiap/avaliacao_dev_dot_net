﻿using System.Linq;
using System.Web.Mvc;

namespace Administrativo.Controllers
{
    public class ClientesController : Controller
    {
        // GET: Clientes
        public ActionResult Index()
        {
            ViewBag.Message = Request.QueryString["msg"]?.ToString();
            ViewBag.Clients = new ServiceReference.ClientServiceClient().GetAll().OrderBy(_ => _.Name).ToList();
            return View();
        }

        public ActionResult Premio(int Cupom)
        {
            ViewBag.Coupon = new CouponServiceReference.CouponServiceClient().Get(Cupom);
            return View();
        }

        [HttpPost]
        public void Novo(string Name, string CPF)
        {
            try
            {
                new ServiceReference.ClientServiceClient().Create(new Domain.Client.Client()
                {
                    Name = Name,
                    CPF = CPF
                });

                Response.Redirect("../Clientes?msg=Success");
            }
            catch (System.Exception)
            {
                Response.Redirect("../Clientes?msg=Error");
            }
        }

        public void Remover(int Id)
        {
            new ServiceReference.ClientServiceClient().Remove(Id);
            Response.Redirect("../Clientes?msg=ClientRemoved");
        }

    }
}