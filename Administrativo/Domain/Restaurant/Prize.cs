﻿using Domain.Client;
using System.Collections.Generic;

namespace Domain.Restaurant
{
    public class Prize
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Client.Client Client { get; set; }
        public Client.Coupon Coupon { get; set; }
        public List<Coupon> Coupons { get; set; }
    }
}
