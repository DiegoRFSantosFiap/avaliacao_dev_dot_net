﻿using System;

namespace Domain.Client
{
    public class Coupon
    {
        public int Id { get; set; }
        public Client Client { get; set; } = new Client();
        public bool IsAwarded { get; set; }
        public DateTime CreationDate { get; set; }
        public Restaurant.Prize Prize { get; set; }
    }
}
