﻿using System.Collections.Generic;

namespace Domain.Client
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CPF { get; set; }
        public List<Restaurant.Prize> Prizes { get; set; } = new List<Restaurant.Prize>();
        private List<Coupon> _coupons { get; set; } = new List<Coupon>();

        public void AddCoupon(Coupon coupon)
        {
            if (this._coupons.Count >= ClientStruct.MAX_COUPONS_BY_CLIENT)
            {
                throw new System.Exception($"O limite é de {ClientStruct.MAX_COUPONS_BY_CLIENT} cupons por usuário");
            }
            this._coupons.Add(coupon);
        }

        public List<Coupon> GetCoupons()
        {
            return this._coupons;
        }
    }
}
