﻿using System;

namespace Domain.User
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Guid Hash { get; set; }
        public bool IsActive { get; set; } = true;
    }
}
