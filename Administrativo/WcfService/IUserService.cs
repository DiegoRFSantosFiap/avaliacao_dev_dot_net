﻿using Domain.User;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.ServiceModel;

namespace WcfService
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        User Create(User user);

        [OperationContract]
        User Get(User user);

        [OperationContract]
        List<User> GetAll(Expression<Func<User, bool>> predicate);

        [OperationContract]
        bool Remove(Expression<Func<User, bool>> predicate);
    }
}
