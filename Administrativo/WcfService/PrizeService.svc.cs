﻿using System.Collections.Generic;
using Domain.Restaurant;
using Data.Repository;

namespace WcfService
{
    public class PrizeService : IPrizeService
    {
        public Prize Create(Prize prize) => new PrizeRepository().Create(prize);

        public Prize Get(Prize prize) => new PrizeRepository().Get(x => x.Equals(prize));

        public List<Prize> GetAll() => new PrizeRepository().GetAll();

        public bool Remove(int prizeId) => new PrizeRepository().Remove(x => x.Id == prizeId);

    }
}
