﻿using System.Collections.Generic;
using Domain.Client;
using Data.Repository;
using System;

namespace WcfService
{
    public class ClientService : IClientService
    {
        public Client Create(Client client) => new ClientRepository().Create(client);

        public List<Client> GetAll() => new ClientRepository().GetAll();

        public Client GetBy(int clientId) => new ClientRepository().Get(_ => _.Id == clientId);

        public bool Remove(int clientId) => new ClientRepository().Remove(_=> _.Id == clientId);
    }
}
