﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Domain.User;
using Data.Repository;

namespace WcfService
{
    public class UserService : IUserService
    {
        public User Create(User user) => new UserRepository().Create(user);

        public User Get(User user) => new UserRepository().Get(_ => _.Equals(user));

        public List<User> GetAll(Expression<Func<User, bool>> predicate) => new UserRepository().GetAll(predicate);

        public bool Remove(Expression<Func<User, bool>> predicate) => new UserRepository().Remove(predicate);
    }
}
