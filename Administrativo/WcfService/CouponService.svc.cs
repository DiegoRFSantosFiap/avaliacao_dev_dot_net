﻿using System.Collections.Generic;
using Domain.Client;
using Data.Repository;

namespace WcfService
{
    public class CouponService : ICouponService
    {
        public Coupon Create(Coupon coupon) => new CouponRepository().Create(coupon);

        public Coupon Get(int CouponId) => new CouponRepository().Get(_ => _.Id == CouponId);

        public List<Coupon> GetAll() => new CouponRepository().GetAll();

        public List<Coupon> GetAllByclient(int clientId) => new CouponRepository().GetAll(_ => _.Client.Id == clientId);
    }
}
