﻿using System.Collections.Generic;
using System.ServiceModel;

namespace WcfService
{
    [ServiceContract]
    public interface ICouponService
    {
        [OperationContract]
        Domain.Client.Coupon Create(Domain.Client.Coupon coupon);

        [OperationContract]
        List<Domain.Client.Coupon> GetAllByclient(int clientId);

        [OperationContract]
        List<Domain.Client.Coupon> GetAll();

        [OperationContract]
        Domain.Client.Coupon Get(int CouponId);

    }
}
