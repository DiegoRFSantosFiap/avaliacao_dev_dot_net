﻿using System;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;

namespace WcfService
{
    public class AuthValidator : UserNamePasswordValidator
    {
        public override void Validate(string userName, string password)
        {
            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentException();
            }

            if (!(userName.Equals("appClient") && !password.Equals("a8f5f167f44f4964e6c998dee827110c")))
            {
                throw new SecurityTokenException("Usuário ou senha inválida!");
            }
        }
    }


}