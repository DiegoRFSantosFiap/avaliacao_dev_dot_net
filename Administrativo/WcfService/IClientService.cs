﻿using System.Collections.Generic;
using System.ServiceModel;

namespace WcfService
{
    [ServiceContract]
    public interface IClientService
    {
        [OperationContract]
        List<Domain.Client.Client> GetAll();

        [OperationContract]
        Domain.Client.Client Create(Domain.Client.Client client);

        [OperationContract]
        Domain.Client.Client GetBy(int ClientId);

        [OperationContract]
        bool Remove(int clientId);
    }
}
