﻿using System.Collections.Generic;
using System.ServiceModel;

namespace WcfService
{
    [ServiceContract]
    public interface IPrizeService
    {
        [OperationContract]
        Domain.Restaurant.Prize Create(Domain.Restaurant.Prize prize);

        [OperationContract]
        Domain.Restaurant.Prize Get(Domain.Restaurant.Prize prize);

        [OperationContract]
        List<Domain.Restaurant.Prize> GetAll();

        [OperationContract]
        bool Remove(int prizeId);
    }
}
