﻿using System.Configuration;
using System.Data.Entity;
using System.Linq;

namespace Data.Context
{
    public class MainContext : DbContext
    {

        public MainContext() : base(ConfigurationManager.ConnectionStrings["MainConnString"].ConnectionString)
        {
            Database.SetInitializer<MainContext>(new CreateDatabaseIfNotExists<MainContext>());
        }

        public DbSet<Domain.Client.Client> Clients { get; set; }
        public DbSet<Domain.Client.Coupon> Coupons { get; set; }
        public DbSet<Domain.Restaurant.Prize> Prizes { get; set; }
        public DbSet<Domain.User.User> Users { get; set; }
    }
}
