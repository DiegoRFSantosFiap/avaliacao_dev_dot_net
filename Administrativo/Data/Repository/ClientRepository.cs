﻿using Data.Context;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Domain.Client;
using System.Linq;

namespace Data.Repository
{
    public class ClientRepository : ICrudRepository<Domain.Client.Client>
    {
        public Client Create(Client obj)
        {
            using (MainContext context = new MainContext())
            {
                if (context.Clients.FirstOrDefault(_ => _.CPF.Equals(obj.CPF)) != null)
                {
                    throw new Exception("Já existe um cliente com este CPF!");
                }

                obj = context.Clients.Add(obj);
                context.SaveChanges();
                return obj;
            };
        }

        public Client Get(Expression<Func<Client, bool>> predicate)
        {
            Client client = null;
            using (MainContext context = new MainContext())
            {
                client = context.Clients.First(predicate);
            };

            return client;
        }

        public List<Client> GetAll(Expression<Func<Client, bool>> predicate = null)
        {
            var clients = new List<Domain.Client.Client>();
            using (MainContext context = new MainContext())
            {
                clients.AddRange(context.Clients);
            };

            return clients;
        }

        public bool Remove(Expression<Func<Client, bool>> predicate)
        {
            try
            {
                using (MainContext context = new MainContext())
                {
                    context.Clients.Remove(context.Clients.First(predicate));
                    context.SaveChanges();
                };
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
