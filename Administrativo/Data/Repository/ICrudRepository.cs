﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Data.Repository
{
    interface ICrudRepository<T>
    {
        T Create(T obj);
        T Get(Expression<Func<T, bool>> predicate);
        List<T> GetAll(Expression<Func<T, bool>> predicate = null);
        bool Remove(Expression<Func<T, bool>> predicate);
    }
}
