﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Client;
using Data.Context;

namespace Data.Repository
{
    public class CouponRepository : ICrudRepository<Coupon>
    {
        public Coupon Create(Coupon obj)
        {
            Coupon coupon = null;
            using (MainContext context = new MainContext())
            {
                if (context.Coupons.Where(_ => _.Client.Id == obj.Client.Id).Count() >= ClientStruct.MAX_COUPONS_BY_CLIENT)
                {
                    throw new Exception($"O máximo de {ClientStruct.MAX_COUPONS_BY_CLIENT} cupons por cliente");
                }

                context.Clients.Attach(obj.Client);

                if (context.Prizes.Count() > 0)
                {
                    obj.IsAwarded = new Random().Next(100) <= 40;
                }

                coupon = context.Coupons.Add(obj);

                if (obj.IsAwarded)
                {
                    obj.Prize = PrizeRepository.GetRandomPrize();
                    context.Prizes.Attach(obj.Prize);
                }

                context.SaveChanges();
            };

            return coupon;
        }

        public Coupon Get(Expression<Func<Coupon, bool>> predicate)
        {
            Coupon coupon = null;
            using (MainContext context = new MainContext())
            {
                coupon = context.Coupons.Include("Prize").First(predicate);
            };

            return coupon;
        }

        public List<Coupon> GetAll(Expression<Func<Coupon, bool>> predicate = null)
        {
            List<Coupon> coupons = new List<Coupon>();
            using (MainContext context = new MainContext())
            {
                if (predicate != null)
                {
                    coupons.AddRange(context.Coupons.Include("Prize").Include("Client").Where(predicate));
                }
                else
                {
                    coupons.AddRange(context.Coupons.Include("Prize").Include("Client").ToList());
                }
            };

            return coupons;
        }

        public bool Remove(Expression<Func<Coupon, bool>> predicate)
        {
            try
            {
                using (MainContext context = new MainContext())
                {
                    context.Coupons.Remove(context.Coupons.First(predicate));
                    context.SaveChanges();
                };

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
