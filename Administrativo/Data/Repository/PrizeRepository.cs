﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.Restaurant;
using Data.Context;

namespace Data.Repository
{
    public class PrizeRepository : ICrudRepository<Domain.Restaurant.Prize>
    {
        public Prize Create(Prize obj)
        {
            using (MainContext context = new MainContext())
            {
                obj = context.Prizes.Add(obj);
                context.SaveChanges();
            };

            return obj;
        }

        public Prize Get(Expression<Func<Prize, bool>> predicate)
        {
            Prize prize = null;
            using (MainContext context = new MainContext())
            {
                prize = context.Prizes.First(predicate);
            };

            return prize;
        }

        public List<Prize> GetAll(Expression<Func<Prize, bool>> predicate = null)
        {
            List<Prize> prizes = new List<Prize>();
            using (MainContext context = new MainContext())
            {
                if (predicate != null)
                {
                    prizes.AddRange(context.Prizes.Include("Client").Where(predicate));
                }
                else
                {
                    prizes.AddRange(context.Prizes.Include("Client").ToList());
                }
            };

            return prizes;
        }

        public bool Remove(Expression<Func<Prize, bool>> predicate)
        {
            try
            {
                using (MainContext context = new MainContext())
                {
                    context.Prizes.Remove(context.Prizes.First(predicate));
                    context.SaveChanges();
                };
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public static Prize GetRandomPrize()
        {
            Prize prize = null;
            using (MainContext context = new MainContext())
            {
                var prizes = context.Prizes.ToList();
                Random rdn = new Random(DateTime.Now.Millisecond);
                prize = prizes[(rdn.Next(prizes.Count))];
            };

            return prize;
        }
    }
}
