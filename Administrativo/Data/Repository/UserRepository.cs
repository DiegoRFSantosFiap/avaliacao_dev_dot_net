﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.User;
using Data.Context;

namespace Data.Repository
{
    public class UserRepository : ICrudRepository<User>
    {
        public User Create(User obj)
        {
            using (MainContext context = new MainContext())
            {
                obj = context.Users.Add(obj);
            };

            return obj;
        }

        public User Get(Expression<Func<User, bool>> predicate)
        {
            User user = null;
            using (MainContext context = new MainContext())
            {
                user = context.Users.First(predicate);
            };

            return user;
        }

        public List<User> GetAll(Expression<Func<User, bool>> predicate = null)
        {
            List<User> users = null;
            using (MainContext context = new MainContext())
            {
                users.AddRange(context.Users.Where(predicate));
            };

            return users;
        }

        public bool Remove(Expression<Func<User, bool>> predicate)
        {
            try
            {
                using (MainContext context = new MainContext())
                {
                    context.Users.Remove(context.Users.First(predicate));
                };
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
